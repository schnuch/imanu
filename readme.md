# imanu
#### In-browser image manipulation

[Get it here for Firefox](https://addons.mozilla.org/addon/imanu/ "https://addons.mozilla.org/addon/imanu/").

The possible manipulations are very basic right now.
More features are planned.

## Demo
You can see a quick demo right [here](https://schnuch.gitlab.io/imanu-demo.html).

![Preview](imanu.jpg)

## Preview as default
The preview of any manipulations to the image are disabled by default as of now. You can turn it on with a single change at the start of the script, where you'll find further info.

**Update**:
There is now an option to save the desired behavior.

## License
[MPLv2.0](https://www.mozilla.org/en-US/MPL/2.0/)
